#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include <vector>
#include <memory>

#include "BarnesHutTree.hpp"
#include "Body.hpp"

namespace nbody {
	//simulation superclass
	class Simulation {
	protected:
		std::vector<Body> bodies;
		std::unique_ptr<BarnesHutTree> tree;
		std::size_t parallelSize{ 0 };
		std::size_t parallelRank{ 0 };
		bool correctState{ false };
	public:
		virtual ~Simulation() = default;
		virtual std::vector<Body> getBodies() const;
		virtual std::size_t getProcessId() const = 0;
		virtual bool readInputData(const std::string& filename);
		virtual void runStep() = 0;
	};
} // namespace nbody

#endif

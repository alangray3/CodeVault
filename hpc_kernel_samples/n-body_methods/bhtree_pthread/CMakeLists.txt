# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Paul Heinzlreiter <paul.heinzlreiter@risc-software.at>
#
# ==================================================================================================

cmake_minimum_required(VERSION 3.0)
project ("bhtree_pthread")
include(${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 4_nbody)
endif()
set(NAME ${DWARF_PREFIX}_bhtree_pthread)

enable_language(CXX)
message("** Enabling '${NAME}'")

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
set(CXX11 ${COMPILER_SUPPORTS_CXX11})
set(CXX11_FLAGS -std=c++11)
set(CMAKE_CXX_FLAGS "-std=c++11")

set(GCC_DEBUG_COMPILE_FLAGS "-g")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_DEBUG_COMPILE_FLAGS}")

find_package(Threads)

include_directories(src/datastructures src/simulation)

add_subdirectory(src/datastructures)
add_subdirectory(src/simulation)

add_executable(${NAME} src/pthreadmain.cpp)
target_link_libraries(${NAME} datastructures simulation ${CMAKE_THREAD_LIBS_INIT})

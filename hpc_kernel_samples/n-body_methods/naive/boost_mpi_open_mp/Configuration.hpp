#pragma once

#include <cstddef>
#include <getopt.h>
#include <iostream>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include <boost/program_options.hpp>
#include <boost/serialization/serialization.hpp>
#pragma clang diagnostic pop

struct Configuration {
	std::size_t NoIterations{1000};
	std::size_t NoParticles{1000};

	template <class Archive> void serialize(Archive& ar, const unsigned int version) {
		(void)version;
		ar& NoIterations;
		ar& NoParticles;
	}

  private:
	friend class boost::serialization::access;
};
BOOST_IS_MPI_DATATYPE(Configuration) // performance hint allowed for contiguous data

Configuration parseArgs(int argc, char* argv[]) {
	namespace po = boost::program_options;
	Configuration conf{};
	po::options_description desc{"Allowed options"};
	desc.add_options()                                                                        //
	    ("help,h", "produce help message")                                                    //
	    ("particles,p", po::value<std::size_t>(&conf.NoParticles), "number of particles")     //
	    ("iterations,i", po::value<std::size_t>(&conf.NoIterations), "number of iterations"); //

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	if (vm.count("help")) std::cout << desc << '\n';

	return conf;
}

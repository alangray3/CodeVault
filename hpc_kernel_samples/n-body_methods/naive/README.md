# README - Naive n-Body Example

## Description

This example shows an naive n-Body kernel implemented in multiple variants using different technologies.
All variants are designed to run on multiple distributed machines is parallel.

The sample contains the following programs:
 * `chapel`: using Cray Chapel parallel programming language. (local and distributed parallelization via Chapel)
 * `open_mp`: using C++14 and MPI for parallelizing across compute nodes, OpenMP for parallelizing withing a compute node. One MPI Process per compute node.
 * `boost_mpi_open_mp`: using C++14 and Boost.MPI C++ API for parallelizing across compute nodes, OpenMP for parallelizing within a compute node. One MPI Process per compute node.
 * `shared_memory_window`: using C++14 and MPI shared memory windows for parallelizing across compute nodes and within compute nodes. n MPI Processes per compute node.
 
 See the sub programs for more details.
 
## Benchmarks

### Strong scaling and parallelization comparison on Supermuc Phase 1

Strong scaling test on Supermuc Phase 1 with 32k particles with different number of cores and different programs.

Hardware: Supermuc Phase 1 Thin Nodes. Intel Sandy Bridge-EP Xeon E5-2680 8C @2.7GHz, 16 Cores per node.
MPI: Intel MPI 5.1.3.181

Commandline: 

	mpiexec -n {ranks} {program} -i2000 -p32768
	
where `{ranks}` is the number of cores and `{program}` is the binary from [`open_mp`, `boost_mpi_open_mp`, `shared_memory_window`].
As Supermuc Phase 1 Thin Nodes are 8 core dual socket systems, we use for the OpenMP programs 2 ranks (processes) per node with `OMP_NUM_THREADS=8`. For the MPI-only shared memory program 16 tasks per node are used. The idea is, each program can utilize exactly 16 hardware threads per node and are therefore be comparable.

![chart](hpc_kernel_samples/n-body_methods/naive/benchmarks/SupermucChart.PNG)

### Strong scaling and parallelization comparison on Supermuc Phase 2

Strong scaling test on Supermuc Phase 2 with 32k particles with different number of cores and different programs.

Hardware: Supermuc Phase 2 Haswell Nodes. Intel Haswell Xeon E5-2697 v3 @2.6GHz, 28 Cores per node.
MPI: Intel MPI 5.1.3.181

Commandline:
	mpiexec -n {ranks} {program} -i2000 -p32768
	
where `{ranks}` is the number of cores and `{program}` is the binary from [`open_mp`, `shared_memory_window`].
Supermuc Phase 2 Haswell Nodes are 14 core dual socket systems. But they have improved socket to socket communication. So we use for the OpenMP programs 1 rank (process) per node with `OMP_NUM_THREADS=28`. For the MPI-only shared memory program 28 tasks per node are used. The idea is, each program can utilize exactly 28 hardware threads per node and are therefore be comparable.

![chart](hpc_kernel_samples/n-body_methods/naive/benchmarks/SupermucHaswellChart.PNG)
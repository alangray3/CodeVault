record Vec3 {
	var x, y, z: real;
}

proc +=(ref a: Vec3, const ref b: Vec3) {
	a.x += b.x;
	a.y += b.y;
	a.z += b.z;
}

proc -=(ref a: Vec3, const ref b: Vec3) {
	a.x -= b.x;
	a.y -= b.y;
	a.z -= b.z;
}

proc *=(ref a: Vec3, const ref b: Vec3) {
	a.x *= b.x;
	a.y *= b.y;
	a.z *= b.z;
}

proc *=(ref a: Vec3, const b: real) {
	a.x *= b;
	a.y *= b;
	a.z *= b;
}

proc /=(ref a: Vec3, const b: real) {
	a.x /= b;
	a.y /= b;
	a.z /= b;
}

proc Distance(const ref v1: Vec3, const ref v2: Vec3) {
	return Norm(v1 - v2);
}

proc Norm(const ref v:Vec3) {
	return sqrt(v.x**2 + v.y**2 + v.z**2);
}

proc Normalize(ref v:Vec3) {
	v /= Norm(v);
}

proc +(in a: Vec3, const ref b: Vec3) {
	a -= b;
	return a;
}

proc -(in a: Vec3, const ref b: Vec3) {
	a -= b;
	return a;
}

proc *(in a: Vec3, const ref b: Vec3) {
	a *= b;
	return a;
}

proc *(in a: Vec3, const b: real) {
	a *= b;
	return a;
}

proc /(in a: Vec3, const b: real) {
	a /= b;
	return a;
}

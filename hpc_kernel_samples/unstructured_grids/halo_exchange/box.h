#ifndef BOX_H
#define BOX_H

enum dimension_aliases { X, Y, Z, N_DIMS };

typedef struct
{
   int coords[N_DIMS];
   int extents[N_DIMS];
   long volume;
} box_t;

// Meta-Information for MPI-Type creation
#define BOX_T_N_INT_MEMBERS 2 * N_DIMS
#define BOX_T_FIRST_INT_MEMBER coords[0]
#define BOX_T_N_LONG_MEMBERS 1
#define BOX_T_FIRST_LONG_MEMBER volume

void box_set_volume(box_t *b);

void box_intersect(const box_t *a, const box_t *b, box_t *intersection);

int box_is_empty(const box_t *b);

void box_split(const box_t *box, box_t *a, box_t *b, int randomize);

void box_decompose(const box_t *original, int n_boxes, box_t *boxes);

void box_grow(box_t *box, int extra_width);

char *box_to_string(const box_t *box, char *buf, int size);

#endif


# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Cedric Nugteren <cedric.nugteren@surfsara.nl>
#
# ==================================================================================================

# FFT with the FFTW3 library
set(NAME ${DWARF_PREFIX}_fftw)
if (FFTW_FOUND)
  include_directories(${FFTW_INCLUDE_DIRS})
  add_executable(${NAME} fftw/fftw.cc)
  target_link_libraries(${NAME} ${FFTW_LIBRARIES})
  install(TARGETS ${NAME} DESTINATION bin)
  message("** Enabling '${NAME}'")
else()
  message("** Skipping '${NAME}': no FFTW3")
  install(CODE "MESSAGE(\"${NAME} can only be built with FFTW3.\")")                                     
endif()
unset(NAME)

# ==================================================================================================

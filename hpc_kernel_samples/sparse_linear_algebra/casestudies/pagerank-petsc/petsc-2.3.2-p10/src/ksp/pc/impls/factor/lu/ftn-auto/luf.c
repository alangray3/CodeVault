#include "petsc.h"
#include "petscfix.h"
/* lu.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscpc.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorreorderfornonzerodiagonal_ PCFACTORREORDERFORNONZERODIAGONAL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorreorderfornonzerodiagonal_ pcfactorreorderfornonzerodiagonal
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetfill_ PCFACTORSETFILL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetfill_ pcfactorsetfill
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetuseinplace_ PCFACTORSETUSEINPLACE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetuseinplace_ pcfactorsetuseinplace
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetpivoting_ PCFACTORSETPIVOTING
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetpivoting_ pcfactorsetpivoting
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define pcfactorsetpivotinblocks_ PCFACTORSETPIVOTINBLOCKS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define pcfactorsetpivotinblocks_ pcfactorsetpivotinblocks
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   pcfactorreorderfornonzerodiagonal_(PC pc,PetscReal *rtol, int *__ierr ){
*__ierr = PCFactorReorderForNonzeroDiagonal(
	(PC)PetscToPointer((pc) ),*rtol);
}
void PETSC_STDCALL   pcfactorsetfill_(PC pc,PetscReal *fill, int *__ierr ){
*__ierr = PCFactorSetFill(
	(PC)PetscToPointer((pc) ),*fill);
}
void PETSC_STDCALL   pcfactorsetuseinplace_(PC pc, int *__ierr ){
*__ierr = PCFactorSetUseInPlace(
	(PC)PetscToPointer((pc) ));
}
void PETSC_STDCALL   pcfactorsetpivoting_(PC pc,PetscReal *dtcol, int *__ierr ){
*__ierr = PCFactorSetPivoting(
	(PC)PetscToPointer((pc) ),*dtcol);
}
void PETSC_STDCALL   pcfactorsetpivotinblocks_(PC pc,PetscTruth *pivot, int *__ierr ){
*__ierr = PCFactorSetPivotInBlocks(
	(PC)PetscToPointer((pc) ),*pivot);
}
#if defined(__cplusplus)
}
#endif

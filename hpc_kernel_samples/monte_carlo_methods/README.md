# README - Monte Carlo Methods

# Folders:
 1. integral_basic
 1. pi
 1. prng
 1. prime

## Integral Basic

### Files:
1. integral1d.c
1. integral1d_mpi.c
1. integral1d_OMP.c

#### integral1d.c

* This example computes integral of ( 1 / (1 + x * x) ) in x=[0,1] by using importance sampling Monte Carlo method

* This code is a serial algorithm, no need for any external library

* This is one of the code sample from the PRACE CodeVault. You can find more code samples available for download from the PRACE CodeVault here: https://gitlab.com/PRACE-4IP/CodeVault

* **Relase Date:** 28 January 2016

* **Version:** 1.0 (initial version)

* **Contributors:** Ahmet Tuncer DURAK (UHeM, ITU, TURKEY), Samet DEMIR (ITU, TURKEY)

* **Copyright:** None. The code is free to copy, modify and use, but comes with no warranty and therefore usage is at your own risk.

* **Language:** This code sample file is written in C. No external libraries are required.

* **Parallelisation Implementation:** This file is a serial code

* **Level of the code sample complexity:** Code sample complexity is simple and intended for new starters.

* **Instructions on how to compile the code:** Load a supported programming environment or compiler [GNU, PGI, Intel, Clang], Use the CodeVault CMake infrastructure, see main README.md

* **Instructions on how to run the code:** ./7_montecarlo_integral1d_serial trial#

* **Sample input:** There is one already in the code (1 / (1 + x * x)), can be changed from the code.

* **Sample output:** For a trial of 100 output is
	computed value of the integral= 0.784455 
	true value of the integral=     0.785398 
	error=                          0.000943


#### integral1d_mpi.c

* This code sample computes integral of ( 1 / (1 + x * x) ) in x=[0,1] by 	using importance sampling Monte Carlo method

* A MPI framework is needed to run the code

* This is one of the code sample from the PRACE CodeVault. You can find more code samples available for download from the PRACE CodeVault here: https://gitlab.com/PRACE-4IP/CodeVault

* **Relase Date:** 28 January 2016

* **Version: 1.0** (initial version)

* **Contributors:** Ahmet Tuncer DURAK (UHeM, ITU, TURKEY), Samet DEMIR (ITU, TURKEY)

* **Copyright:** None. The code is free to copy, modify and use, but comes with no warranty and therefore usage is at your own risk.

* **Language:** This code sample file is written in C.

* **Parallelisation Implementation:** MPI

* **Level of the code sample complexity:** Code sample complexity is simple and intended for new starters.

* **Instructions on how to compile the code:** Load a supported programming environment or compiler [GNU, PGI, Intel, Clang], Use the CodeVault CMake infrastructure, see main README.md

* **Instructions on how to run the code:** mpirun -n 4 ./7_montecarlo_integral1d_serial #trial (for 4 process)

* **Sample input:** mpirun -n 4 ./7_montecarlo_integral1d_serial 1000

* **Sample output:** For a trial of 100 output is
	computed value of the integral= 0.784455 
	true value of the integral=     0.785398 
	error=                          0.000943

#### integral1d_OMP.c

* This code sample computes integral of ( 1 / (1 + x * x) ) in x=[0,1] by 	using importance sampling Monte Carlo method

* Additional pre-requisites: OpenMP

* **Relase Date:** 28 January 2016

* **Version:** 1.0 (initial version)

* **Contributors:** Ahmet Tuncer DURAK (UHeM, ITU, TURKEY), Samet DEMIR (ITU, TURKEY)

* **Copyright:** None. The code is free to copy, modify and use, but comes with no warranty and therefore usage is at your own risk.

* **Language:** This code sample file is written in C.

* **Parallelisation Implementation:** OpenMP

* **Level of the code sample complexity:** Code sample complexity is simple and intended for new starters.

* **Instructions on how to compile the code:** Load a supported programming environment or compiler [GNU, PGI, Intel, Clang], Use the CodeVault CMake infrastructure, see main README.md

* **Instructions on how to run the code:** ./7_montecarlo_integral1d_openmp thread# trial#

* **Sample input:** ./7_montecarlo_integral1d_openmp 4 10000

* **Sample output:** For a trial of 100 output is
	computed value of the integral= 0.784455 
	true value of the integral=     0.785398 
	error=                          0.000943

## Pi
	
### Files
1. pi_mpi.c
1. pi_omp.c
1. pi_serial.c
1. pi_shared.c
1. pi_shared.h

#### pi_serial.c

* This code sample calculates the value of pi by using Monte Carlo Methods.
 
* This code is a serial algorithm, no need for any external library
 
* This is one of the code sample from the PRACE CodeVault. You can 	find more code samples available for download from the PRACE 	CodeVault here: https://gitlab.com/PRACE-4IP/CodeVault
 
* **Relase Date:** 28 January 2016
 
* **Version:** 1.0 (initial version)
 
* **Contributors:** Ahmet Tuncer DURAK (UHeM, ITU, TURKEY), Samet DEMIR (	ITU, TURKEY)
 
* **Copyright:** None. The code is free to copy, modify and use, but comes 	with no warranty and therefore usage is at your own risk.
 
* **Language:** This code sample file is written in C. No external 	libraries are required.
 
* **Parallelisation Implementation:** This file is a serial code
 
* **Level of the code sample complexity:** Code sample complexity is 	simple and intended for new starters.
 
* **Instructions on how to compile the code:** Load a supported programming environment or compiler [GNU, PGI, Intel, Clang], Use the CodeVault CMake infrastructure, see main README.md
 
* **Instructions on how to run the code:** ./7_montecarlo_pi_serial seed# N

 	seed (integer): seed for the pseudo random number generator
          a negative value indicates current timestamp
	N (integer)   : number of random samples to use

* **Sample input:** ./7_montecarlo_pi_serial 10 100000

* **Sample output:** 10	100000	1	3.142156	0.000563 (seed N thread calculated_pi error)

#### pi_omp.c

* This code sample calculates the value of pi by using Monte Carlo Methods.
 
* **Additional pre-requisites:** OpenMP
 
* This is one of the code sample from the PRACE CodeVault. You can 	find more code samples available for download from the PRACE 	CodeVault here: https://gitlab.com/PRACE-4IP/CodeVault
 
* **Relase Date:** 28 January 2016
 
* **Version:** 1.0 (initial version)
 
* **Contributors:** Ahmet Tuncer DURAK (UHeM, ITU, TURKEY), Samet DEMIR (	ITU, TURKEY)
 
* **Copyright:** None. The code is free to copy, modify and use, but comes 	with no warranty and therefore usage is at your own risk.
 
* **Language:** This code sample file is written in C. No external 	libraries are required.
 
* **Parallelisation Implementation:** OpenMP
 
* **Level of the code sample complexity:** Code sample complexity is 	simple and intended for new starters.
 
* **Instructions on how to compile the code:** Load a supported programming environment or compiler [GNU, PGI, Intel, Clang], Use the CodeVault CMake infrastructure, see main README.md
 
* **Instructions on how to run the code:** ./7_montecarlo_pi_omp seed# N

 	seed (integer): seed for the pseudo random number generator
          a negative value indicates current timestamp
	N (integer)   : number of random samples to use

* **Sample input:** ./7_montecarlo_pi_serial 10 100000

* **Sample output:** 10	100000	1	3.142156	0.000563 (seed N thread calculated_pi error)


#### pi_mpi.c

* This code sample calculates the value of pi by using Monte Carlo Methods.
 
* **Additional pre-requisites:** MPI 
 
* This is one of the code sample from the PRACE CodeVault. You can 	find more code samples available for download from the PRACE 	CodeVault here: https://gitlab.com/PRACE-4IP/CodeVault
 
* **Relase Date:** 28 January 2016
 
* **Version:** 1.0 (initial version)
 
* **Contributors:** Ahmet Tuncer DURAK (UHeM, ITU, TURKEY), Samet DEMIR (	ITU, TURKEY)
 
* **Copyright:** None. The code is free to copy, modify and use, but comes 	with no warranty and therefore usage is at your own risk.
 
* **Language:** This code sample file is written in C. No external 	libraries are required.
 
* **Parallelisation Implementation:** MPI
 
* **Level of the code sample complexity:** Code sample complexity is 	simple and intended for new starters.
 
* **Instructions on how to compile the code:** Load a supported programming environment or compiler [GNU, PGI, Intel, Clang], Use the CodeVault CMake infrastructure, see main README.md
 
* **Instructions on how to run the code:** mpirun -n 4 ./7_montecarlo_pi_omp seed# N# (for 4 process)

 	seed (integer): seed for the pseudo random number generator
          a negative value indicates current timestamp
	N (integer)   : number of random samples to use

* **Sample input:** mpirun -n 4 ./7_montecarlo_pi_omp 10 10000

* **Sample output:** 10	100000	1	3.142156	0.000563 (seed N thread calculated_pi error)


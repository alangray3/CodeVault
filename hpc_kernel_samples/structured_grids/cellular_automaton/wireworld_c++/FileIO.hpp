#pragma once

#include <cstddef>
#include <string>
#include <tuple>

#include "MpiEnvironment.hpp"
#include "MpiSubarray.hpp"
#include "State.hpp"
#include "Util.hpp"

struct HeaderInfo {
	Size GlobalSize;
	std::size_t HeaderLength;
};

struct FileIO {
	static HeaderInfo ReadHeader(const std::string& path);

	static void WriteHeader(const HeaderInfo& header, const std::string& path,
	                        const MpiEnvironment& env);

	static TileInfo GetTileInfo(Size globalSize, Size procsSize,
	                            std::size_t rank);

	// helper class to share commonly used data for reading and writing
	class Tile {
		static constexpr std::size_t LF = 1; // linefeed chars
		const std::string& path_;
		const std::size_t headerLength_;
		const Size srcSize_;
		const Size procsSize_;
		State* buf_;

		const TileInfo tileInfo_;
		const Size tileSize_;
		const Coord tileCoord_;
		const MpiSubarray tileType_;
		const MpiSubarray bufType_;
		const std::size_t displ_;

	  public:
		Tile(const std::string& path, HeaderInfo header, Size procsSize,
		     std::size_t rank, State* buf);

		void Read();
		void Write() const;
	};
};

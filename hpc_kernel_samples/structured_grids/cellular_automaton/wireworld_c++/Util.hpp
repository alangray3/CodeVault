#pragma once

#include <cstddef>
#include <string>

struct Size {
	std::size_t Cols{};
	std::size_t Rows{};
};

struct Coord {
	std::size_t X{};
	std::size_t Y{};
	Coord(std::size_t x, std::size_t y) : X(x), Y(y) {}
};

struct TileInfo {
	::Size Size;
	::Coord GlobalCoord;
	::Coord ProcCoord;
};

[[noreturn]] void MpiReportErrorAbort(const std::string& err);
